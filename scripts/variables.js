// Una linea de comentario

/**
 * Un bloque de comentario
 * 
 * 
 * Mas cosas
 */

 /**
  * Javascript !== Java
  * 
  * 1. Lenguaje orientado a objetos
  * 2. Lenguaje de alto nivel
  * 3. Lenguaje interpretado
  */

// Variables:
const number = 1;
const integer = 12345;
const float = 123.2356789;
const string = 'Me llamo Ricardo pero me dicen "Talo".';
const array = [1, 123.4, 'Hola', ['hola', 1]];
const object = {
    name: 'Talo',
    age: 27,
    gender: 'male',
};

const value1 = 5;
const value2 = 2;

function sum(primerNumero, segundoNumero) {
    return primerNumero + segundoNumero;
}

// sum(value1, value2); // 3;

function sum() {
    return 1 + 2;
}