/**
 * Escuchar cuando el usuario haga click en 'Publicar' ✔️
 * Ir a traer el input ✔️
 * Tomar el valor que el usuario ingreso en el input ✔️
 * Ir a traer el contenedor de los comentarios  ✔️
 * Agregar el valor del usuario en el contenedor ✔️
 */


// Escuchar cuando el usuario haga click en 'Publicar'
const postButton = document.querySelector('#post-comment');
const commentInput = document.querySelector('#comment-input');
const commentFeed = document.querySelector('#comment-feed');

//Agregar funcionalidad al boton de like
function addEventToButtons() {
    const buttons = document.querySelectorAll('.comment-like');

    buttons.forEach(function(currentButton) {
        currentButton.addEventListener('click', function() {
            const icon = currentButton.querySelector('i');
            const isActive = icon.classList.contains('fas');
            
            currentButton.classList.toggle('comment-like-active');

            if (isActive) {
                icon.classList.replace('fas', 'far');
            } else {
                icon.classList.replace('far', 'fas');
            }
        });
    });
}

postButton.addEventListener('click', function() {
    const userInput = document.createTextNode(commentInput.value);

    const commentContainer = document.createElement('div');
    commentContainer.className = 'comment';

    const commentImgContainer = document.createElement('div');
    commentImgContainer.className = 'comment-img-container';

    const userAvatar = document.createElement('img');
    userAvatar.className = 'user-avatar';
    userAvatar.setAttribute('src', 'https://scontent-mia3-2.cdninstagram.com/v/t51.2885-19/s150x150/22582088_735399073336517_7044282068120895488_n.jpg?_nc_ht=scontent-mia3-2.cdninstagram.com&_nc_ohc=Ws7h4whnUQgAX_p0RMd&oh=d3307fe00d1fe04244523abbf2e8adea&oe=5E897729')

    commentImgContainer.appendChild(userAvatar);
    commentContainer.appendChild(commentImgContainer);
    commentContainer.appendChild(userInput);
    commentFeed.appendChild(commentContainer);

    commentInput.value = '';

    addEventToButtons();
});

addEventToButtons();

function myFunction() {
    //
}
